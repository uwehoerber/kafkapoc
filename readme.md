
# kleiner Kafka POC

## setup vm
Download virtual box

Download centos image

Create VM in virtual box

Start VM with centos image as boot medium (you will be asked after pressing start)

Install centos (enable network connection during installation)

create a user besides root if so desired


## Setup Kafka
Download tar from https://kafka.apache.org/downloads

unpack


## Setup Zookeeper
Download https://apache.org/dist/zookeeper/

unpack


## Start
create a folder and declar it in kafka/config/server.config for logs

create a folder and declar it in kafka/config/zookeeper.config for logs

2 screens für die server öffnen (siehe linux screen)

use kafka/bin/zookeeper-server-start.sh
use kafka/bin/kafka-server-start.sh

create the "test" topic in kafka


## Ports
Enable portforwarding for your VM
Vm -> settings -> network -> port forwarding
9092 guest system -> 9092 host system

allow tcp22 and tcp9092 in vm


## start service =)
/send takes a json object with a message parameter and sends it
/listen listens on the test subject
