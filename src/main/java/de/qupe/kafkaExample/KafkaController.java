package de.qupe.kafkaExample;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaController {

  public static String KAFKA_BROKERS = "localhost:9092";
  public static String TOPIC_NAME = "test";

  @PostMapping("send")
  public void sendMessage(@RequestBody SendDTO message) {

    Properties props = new Properties();

    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_BROKERS);
    props.put(ProducerConfig.ACKS_CONFIG, "all");
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    
    try (KafkaProducer<String, String> producer = new KafkaProducer<String, String>(props)) {
      producer.send(new ProducerRecord<String, String>(TOPIC_NAME, message.message)).get();
    } catch (ExecutionException e) {
      System.out.println("Error in sending record");
      System.out.println(e);
    } catch (InterruptedException e) {
      System.out.println("Error in sending record");
      System.out.println(e);
    }
 
  }

  @PostMapping("listen")
  public void startListening() {

    new Thread(new Runnable() {

      @Override
      public void run() {
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_BROKERS);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "console-consumer-myapp");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        ArrayList<String> topics = new ArrayList<String>();
        topics.add(TOPIC_NAME);
        consumer.subscribe(topics);

        int noMessageFound = 0;
        while (true) {
          ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofMillis(1000));
          // 1000 is the time in milliseconds consumer will wait if no record is found at broker.
          if (consumerRecords.count() == 0) {
            noMessageFound++;
            if (noMessageFound > 50) {
              // If no message found count is reached to threshold exit loop.
              break;
            } else {
              continue;
            }
          }
          // print each record.
          consumerRecords.forEach(record -> {
            System.out.println("Record Key " + record.key());
            System.out.println("Record value " + record.value());
            System.out.println("Record partition " + record.partition());
            System.out.println("Record offset " + record.offset());
          });
          // commits the offset of record to broker.
          consumer.commitAsync();
        }
        consumer.close();
      }
    }).start();

  }

}
